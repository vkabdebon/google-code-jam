import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Main {

	public static class Chest {
		int keyToOpen;
		int[] containKeys;
		int id;

		public boolean canOpen(Set<Integer> keySet) {
			for (Integer key : keySet) {
				if (key == keyToOpen) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner a = new Scanner(System.in);

		int testCases = Integer.parseInt(a.nextLine());

		for (int testIndex = 0; testIndex < testCases; testIndex++) {

			String[] tokens = a.nextLine().split(" ");
			int numberOfKeys = Integer.parseInt(tokens[0]);
			int numberOfChests = Integer.parseInt(tokens[1]);
			String[] keyStartWith = a.nextLine().split(" ");
			Set<Integer> startingKeys = new HashSet<Integer>();
			for (String keyStart : keyStartWith) {
				startingKeys.add(Integer.parseInt(keyStart));
			}
			List<Chest> chestSet = new ArrayList<Chest>();
			String[] chests = new String[numberOfChests];
			for (int chestToOpen = 0; chestToOpen < Integer.parseInt(tokens[1]); chestToOpen++) {
				String chestInfo = a.nextLine();
				chests[chestToOpen] = chestInfo;
				String[] chestTokens = chestInfo.split(" ");
				int keyNeeded = Integer.parseInt(chestTokens[0]);
				int[] keys = new int[Integer.parseInt(chestTokens[1])];

				for (int i = 0; i < Integer.parseInt(chestTokens[1]); i++) {
					keys[i] = Integer.parseInt(chestTokens[2 + i]);
				}
				Chest c = new Chest();
				c.id = chestToOpen + 1;
				c.containKeys = keys;
				c.keyToOpen = keyNeeded;
				chestSet.add(c);
			}
			System.out.print("Case #" + (testIndex+1) + ": ");
			List<Integer> chestsOrder = solveChests(startingKeys, chestSet);
			if (chestsOrder.size() == 0) {
				System.out.print("IMPOSSIBLE");
			} else {
				for (Integer chestNumber : chestsOrder) {
					System.out.print(chestNumber+" ");
				}
			}
			System.out.println();

		}
		System.out.println("End of program.");
	}

	private static List<Integer> solveChests(Set<Integer> availableKeys,
			List<Chest> chests) {
		ArrayList<Integer> chestOrder = new ArrayList<Integer>();
		while (chests.size() != 0) {
			HashSet<Integer> keyAtThisRound = new HashSet<>(availableKeys);
			List<Integer> chestOpenedThisRound = new ArrayList<>();
			int availableKeySize = availableKeys.size();
			List<Chest> temp = new ArrayList<Chest>(chests);
			for (Chest c : temp) {
				if (c.canOpen(keyAtThisRound)) {
					chests.remove(c);
					for (int newKey : c.containKeys) {
						availableKeys.add(newKey);
					}
					chestOpenedThisRound.add(c.id);
				}
			}
			int newAvailableKeySize = availableKeys.size();
			if (chests.size() != 0 && availableKeySize == newAvailableKeySize) {
				return new ArrayList<Integer>();
			}
			Collections.sort(chestOpenedThisRound, new Comparator<Integer>(){
				@Override
				public int compare(Integer arg0, Integer arg1) {
					// TODO Auto-generated method stub
					return - Integer.compare(arg0, arg1);
				}
			});
			chestOrder.addAll(chestOpenedThisRound);
		}
		return chestOrder;
	}

}
